#!/usr/bin/python2
import sys
import os
import subprocess
from netaddr import *

#ipAddress = raw_input("Please enter the IP address:")
iprange = raw_input("Enter IP range:")
ipAddress = IPNetwork(iprange)

ip_list = list(ipAddress)
len(ip_list)

from subprocess import Popen, PIPE

poodle_file = open('poodle.txt', 'w')
poodle_error = open('poodle_error.txt', 'w')

handshake = "handshake"

for ip in ip_list:
	print(ip)
	process = Popen(['openssl', 's_client', '-connect', '{}:443'.format(ip), '-ssl3'], stdout=PIPE)
	output, err = process.communicate()
#    output = process.communicate()
#	if err is True:
	if handshake not in err:
		print "NOT VULNERABLE"
	else:
		print "VULNERABLE"
	exit_code = process.wait()
#    print(err)
#	poodle_file.write(output)
#	poodle_file.write('\r\n')
#	poodle_error.write(str(ip))
#	poodle_error.write('\r\n')
#	poodle_error.write(err)
#	poodle_error.write('\r\n')

poodle_file.close()
